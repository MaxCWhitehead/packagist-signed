#!/usr/bin/env php
<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

$satis_config = '/build/satis.json';

try {
  // Load existing config.
  $existing_config = file_get_contents($satis_config);
  $config = json_decode($existing_config);

  // Update homepage domain.
  if ($homepage = getenv('HOMEPAGE_DOMAIN')) {
    $config->homepage = $homepage;
    $config->archive->{'prefix-url'} = $homepage;
  }

  // Reset dynamic config.
  $config->repositories = [];
  $config->{'available-package-patterns'} = [];

  // List packages in the drupal vendor namespace.
  foreach (['drupal', 'php-tuf'] as $namespace) {
    $response = json_decode((new GuzzleHttp\Client())->get('https://packagist.org/packages/list.json', [
      'headers' => ['User-Agent' => 'Drupal packagist-signed https://gitlab.com/drupal-infrastructure/package-signing/packagist-signed'],
      'query' => [
        'vendor' => $namespace,
        'fields[]' => 'repository',
      ],
    ])->getBody()->getContents());
    $config->{'available-package-patterns'}[] = $namespace . '/*';
    foreach ($response->packages as $name => $package) {
      $data = new stdClass();
      $data->name = $name;
      $data->type = 'vcs';
      $data->url = $package->repository;
      $config->repositories[] = $data;
    }
  }
  $new_config = json_encode($config, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

  // Write out if changed.
  if ($new_config !== $existing_config) {
    print 'Updating satis.json' . PHP_EOL;
    file_put_contents($satis_config, $new_config);
  }
}
catch (Exception $e) {
  print 'Exception while checking for satis.json updates: ' . $e->getMessage() . PHP_EOL;
}
