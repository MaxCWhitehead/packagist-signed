#!/usr/bin/env php
<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

use phpseclib3\Net\SFTP;

$output_directory = '/build/output';
$satis_config = $output_directory . '/../satis.json';
$prev_statis_config = $output_directory . '/prev_satis.json';

// On success, set to $last_run_file, indicates we should be up to date with packages
// up to this file's timestamp.'
$last_completed_file = $output_directory . '/last-completed';
$last_run_file = $output_directory . '/last-run';


// From => to.
$files_to_copy = [
  $output_directory . '/packages.json' => 'packages.json',
];

// Get all targets already signed.
$existing_targets = [];
try {
  $tuf_url = getenv('TUF_URL');
  $tuf_client = new GuzzleHttp\Client([
    'base_uri' => $tuf_url . 'metadata/',
    'headers' => ['User-Agent' => 'Drupal packagist-signed https://gitlab.com/drupal-infrastructure/package-signing/packagist-signed'],
  ]);
  $requests = [];
  foreach (json_decode($tuf_client->get('bins.json')->getBody()->getContents())->signed->delegations->roles as $bin) {
    $requests[] = new GuzzleHttp\Psr7\Request('GET', $tuf_url . 'metadata/' . $bin->name . '.json');
  }
  print 'Fetching ' . count($requests) . ' bins…' . PHP_EOL;
  $pool = new GuzzleHttp\Pool($tuf_client, $requests, [
    'concurrency' => 6,
    'fulfilled' => function (GuzzleHttp\Psr7\Response $response) use (&$existing_targets) {
      foreach (json_decode($response->getBody()->getContents())->signed->targets as $name => $target) {
        $existing_targets[$name] = $target->hashes->sha256;
      }
    }
  ]);
  $pool->promise()->wait();
}
catch (Exception $e) {
  print 'Exception while fetching TUF repository: ' . $e->getMessage() . PHP_EOL;
  exit(1);
}

// Process each Packagist metadata file.
$it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($output_directory . '/p2', FilesystemIterator::SKIP_DOTS));
$it->rewind();
while ($it->valid()) {
  print 'Processing ' . $it->key() . PHP_EOL;
  // The file itself.
  $files_to_copy[$it->key()] = preg_replace('#^' . preg_quote($output_directory, '#') . '/#', '', $it->key());
  // Each dist file.
  foreach (json_decode(file_get_contents($it->key()), TRUE)['packages'] as $versions) {
    foreach (Composer\MetadataMinifier\MetadataMinifier::expand($versions) as $version) {
      if (empty($version['dist']['url']) || !is_string($version['dist']['url'])) {
        print 'Dist URL missing ' . $version['version'] . PHP_EOL;
        continue;
      }
      $from = $output_directory . preg_replace('#^.*(?=/dist/)#', '', $version['dist']['url']);
      if (!is_file($from)) {
        // Happens for metapackages, which are not downloaded, so usually okay.
        print 'Not local ' . $version['version'] . ':' . $version['dist']['url'] . PHP_EOL;
        continue;
      }
      $target_name = $version['name'] . '/' . $version['version_normalized'];
      if (isset($existing_targets[$target_name]) && (preg_match('#\.zip$#', $from) || $existing_targets[$target_name] === hash_file('sha256', $from))) {
        print 'Target already exists ' . $version['version'] . ':' . $version['dist']['url'] . PHP_EOL;
        continue;
      }
      $files_to_copy[$from] = $target_name;
    }
  }

  $it->next();
}

// Upload via SFTP.
$suffix = (new DateTimeImmutable('now', new DateTimeZone('UTC')))->format('c') . '_' . getmypid();
$tuf_building_directory = 'post-to-tuf/building_' . $suffix;

$rugged_sftp = getenv('RUGGED_SFTP');
list($username, $hostname) = explode('@', $rugged_sftp);
$sftp = new SFTP($hostname);
if (!$sftp->login($username, phpseclib3\Crypt\PublicKeyLoader::load(file_get_contents('/.ssh/id')))) {
  throw new Exception('SFTP login failed');
}
// In seconds, 3 minutes.
$sftp->setTimeout(3 * 60);

// Create directory structure.
print 'Creating directory structure…' . PHP_EOL;
$sftp->mkdir($tuf_building_directory, -1, TRUE);
foreach (array_unique(array_map('dirname', $files_to_copy)) as $directory) {
  if ($directory === '.') {
    continue;
  }
  $sftp->mkdir($tuf_building_directory . '/' . $directory, -1, TRUE);
}

// Upload files.
foreach ($files_to_copy as $from => $to) {
  print 'Transferring ' . $to . PHP_EOL;
  $sftp->put($tuf_building_directory . '/' . $to, $from, SFTP::SOURCE_LOCAL_FILE);
}

// Ready to process.
$sftp->rename($tuf_building_directory, 'post-to-tuf/tuf_ready_' . $suffix);

print 'Run Complete - Update `last-completed` from `last-run`, update `prev_satis.json`' . PHP_EOL;
touch($last_completed_file, filemtime($last_run_file));
copy($satis_config, $prev_statis_config);
