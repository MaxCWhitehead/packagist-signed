#!/usr/bin/env php
<?php

declare(strict_types=1);

require_once __DIR__ . '/vendor/autoload.php';

$output_path = '/build/output';
$satis_config = $output_path . '/../satis.json';
$prev_statis_config = $output_path . '/prev_satis.json';

// Touched to update timestamp when starting job for last timestamp queried packages
$last_run_file = $output_path . '/last-run';
// On success, set to $last_run_file, indicates we should be up to date with packages
// up to this file's timestamp.'
$last_completed_file = $output_path . '/last-completed';

// Packages updated if a partial update can be done.
$packages_updated_file = $output_path . '/packages-updated';
file_put_contents($packages_updated_file, '');

$update_needed = FALSE;
if (file_exists($last_completed_file)) {
  $last_completed = filemtime($last_completed_file);
  if (time() - $last_completed > 23 * 60 * 60) {
    print 'Last update was over 23 hours ago' . PHP_EOL;
    $update_needed = TRUE;
    touch($last_run_file);
  }
  elseif (!file_exists($prev_statis_config) || (sha1_file($satis_config) != sha1_file($prev_statis_config))) {
    print 'satis.json updated' . PHP_EOL;
    $update_needed = TRUE;
    touch($last_run_file);
  }
  else {
    try {
      $packages_json = file_get_contents($output_path . '/packages.json');
      if ($packages_json === false) {
        print 'packages.json not found - last run likely did not complete successfully.' . PHP_EOL;
        $update_needed = TRUE;
        touch($last_run_file);
      }
      else {
        // Get changes from Packagist.
        $packages_satis = array_column(json_decode(file_get_contents($satis_config))->repositories, 'name');
        print 'Getting changes from Packagist.org since ' . $last_completed . ', looking for ' . count($packages_satis) . 'packages' . PHP_EOL;
        $response = json_decode((new GuzzleHttp\Client())->get('https://packagist.org/metadata/changes.json', [
            'headers' => ['User-Agent' => 'Drupal packagist-signed https://gitlab.com/drupal-infrastructure/package-signing/packagist-signed'],
            'query' => ['since' => $last_completed * 10000],
        ])->getBody()->getContents());

        $packages_updated = [];
        foreach ($response->actions as $action) {
          $package_name = preg_replace('/~dev$/', '', $action->package);
          if (in_array($package_name, $packages_satis)) {
            print $action->package . ' updated' . PHP_EOL;
            $packages_updated[] = $package_name;
            $update_needed = TRUE;
          }
        }

        file_put_contents($packages_updated_file, implode(' ', array_unique($packages_updated)));
        touch($last_run_file, (int) floor($response->timestamp / 10000));
      }
    }
    catch (Exception $e) {
      print 'Exception while checking Packagist.org for changes: ' . $e->getMessage() . PHP_EOL;
    }
  }
}
else {
  print 'Last update time not found' . PHP_EOL;
  $update_needed = TRUE;
  touch($last_run_file);
}

if ($update_needed) {
  exit(0);
}
else {
  print 'No update needed' . PHP_EOL;
  exit(1);
}
