#!/bin/sh

# Exit immediately on uninitialized variable or error, and print each command.
set -uex

/satis/vendor/bin/composer --version
/satis/vendor/bin/composer config -g github-oauth.github.com "${GITHUB_TOKEN}"

/app/update-drupal-config.php

if /app/update-needed.php; then
  /satis/bin/satis build --minify satis.json output --skip-errors ${SATIS_BUILD_EXTRA_ARGS} $(cat /build/output/packages-updated)

  # Remove Composer 1 support.
  rm -rv /build/output/include
  jq 'del(.includes)' < /build/output/packages.json > /build/output/packages-new.json
  mv -v /build/output/packages-new.json /build/output/packages.json

  # Find dist archives to sign.
  /app/process.php
fi
