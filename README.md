A Satis mirror of select Packagist.org Composer packages with TUF signing by rugged.

Run with

```
docker run --rm --init -it --user $(id -u):$(id -g) --volume $(pwd)/satis:/build --volume $(pwd)/composer:/composer --volume $(pwd)/ssh:/.ssh --env GITHUB_TOKEN=${GITHUB_TOKEN} --env TUF_URL=${TUF_URL} --env RUGGED_SFTP=${RUGGED_SFTP} {image}
```

- `GITHUB_TOKEN` a GitHub API token able to read public repositories.
- `TUF_URL` is url for TUF repository outputed by rugged, including trailing /. (e.g. `https://host/`)
- `RUGGED_SFTP` is sftp destination to connect to rugged (e.g. `user@host`)
- `/composer` the Composer cache, should be persisted, not a problem if it disappears.
- `/build` workspace for building files, should be persisted.
- `/.ssh` contains `/.ssh/id`, a private SSH key used to connect to `$rugged_sftp` in `config.php`.

`/build/output` is generated within the container, which will be the Packagist metadata for static hosting.

Run on cron, such as every 5 minutes. Core updates update multiple subtree splits, so running more frequently would require another run.


*Updating `satis.json`*

For Drupal.org’s use case, we want satis.json to contain all packages in the `drupal` vendor namespace from Packagist.org. Update the config with:

```
docker run --rm --init -it --user $(id -u):$(id -g) --volume $(pwd)/satis:/build --entrypoint /app/update-drupal-config.php test-image:latest
```
