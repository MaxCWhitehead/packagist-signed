FROM composer/satis:latest

RUN apk add --no-cache --upgrade jq


# install to /app for running as non-root
RUN mkdir /app
COPY bin /app

RUN addgroup --gid 1001 -S satis && adduser --uid 1001 -S satis -G satis
RUN chown -R satis:satis /app

# /build is working dir, chown to satis user
RUN chown -R satis:satis /build

USER satis

RUN /satis/vendor/bin/composer -d /app install --no-plugins --no-scripts

ENTRYPOINT ["/app/docker-entrypoint.sh"]
